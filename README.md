# README #

Domino OSGi plugin with simple JaxRS based web-service

## Usage ##

Use the following paths:

GET http://<servername>/mypath/rest/helloworld

* mypath - from plugin.xml
* rest - from web.xml
* helloworld - from HelloWorldResource.java


### Takes URL parameter value ###

POST http://<servername>/mypath/rest/helloworld/{param}

### Takes URL parameter and json body value ###

POST http://<servername>/mypath/rest/helloworld/json/{param}
