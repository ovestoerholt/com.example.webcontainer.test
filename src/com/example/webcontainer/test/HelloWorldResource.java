package com.example.webcontainer.test;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;


@Path("/helloworld")
public class HelloWorldResource {
    @GET
    public Response getMessage() {
    	String message = "{\"phrase\": \"Hello World\"}";

    	ResponseBuilder builder = Response.ok(message, MediaType.APPLICATION_JSON);
    	return builder.build();
    }
    
    @POST
    @Path("/{param}")
    public Response postMessage(@PathParam("param") String msg) {
    	System.out.println("Method: postMessage");
    	String output = "{\"message\" : \"" + msg + "\"}";
        return Response.ok(output, MediaType.APPLICATION_JSON).build();
    }

    @POST
    @Path("/json/{param}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response postMessageWithJsonBody(@PathParam("param") String msg, String body) {
    	System.out.println("Method: postMessageWithJsonBody");
    	String output = "{\"message\" : \"" + msg + "\", ";
    	output = output + "\"body\" : " + body + "}";
        return Response.ok(output, MediaType.APPLICATION_JSON).build();
    }
  
} 